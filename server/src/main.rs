use warp::Filter;

const TEST_KEY_PUBLIC_STR: &str = "fad415fbaa0339c4fd372d8287e50f67905321ccfd9c43fa4c20ac40afed1983";
const TEST_KEY_SECRET_STR: &str = "a7e4d1c8be858d683ab9cb15574bd0bc3a87e6c846cdaf848da498909cb574f7";

#[tokio::main]
async fn main() {
    // GET /hello/warp => 200 OK with body "Hello, warp!"
    let hello = warp::path!("hello" / String)
        .map(|name| format!("Hello, {}!", name));

    warp::serve(hello)
        .run(([0, 0, 0, 0], 7878))
        .await;
}

mod filters {
    use super::handlers;
    use super::models::Db;
    use warp::Filter;

    pub fn boards(
        db: Db
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        get(db.clone())
            .or(get_board(db.clone()))
    }

    /// GET /
    /// Why not just let the server respond with the current number of boards
    /// instead of a float that's potentially 0.000000001
    pub fn get(
        db: Db
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone { 
        warp::path::end()
            .and(warp::header::exact("Spring-Version", "83"))
            .and(with_db(db))
            .and_then(handlers::get_difficulty_factor)
    }

    pub fn get_board(
        db: Db
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone { 
        warp::path::param()
            .and(warp::header::exact("Spring-Version", "83"))
            .and(warp::header::optional::<String>("If-Modified-Since"))
            .and(with_db(db))
            .and_then(handlers::get_board)
    }

    fn with_db(db: Db) -> impl Filter<Extract = (Db,), Error = std::convert::Infallible> + Clone {
        warp::any().map(move || db.clone())
    }
}

mod handlers {
    use std::convert::Infallible;
    use super::models::Db;
    use warp::http::{Response, StatusCode};
    use httpdate::parse_http_date;

    pub async fn get_difficulty_factor(db: Db) -> Result<impl warp::Reply, Infallible> {
        let dbm = db.lock().await;
        let difficulty_factor = super::models::calculate_difficulty_factor(&dbm);
        let body = format!("<html><body>Hi from So83!</body></html>");
        let reply = Response::builder()
            .header("Spring-Version", "83")
            .header("Spring-Difficulty", difficulty_factor.to_string())
            .header("Content-Length", body.len())
            .header("Content-Type", "text/html;charset=utf-8")
            .body(body);
        Ok(reply)
    }

    pub async fn get_board(
        key: String
      , if_modified_since: Option<String>
      , db: Db
      ) -> Result<impl warp::Reply, Infallible> {
        // First check if the key is even parsable
        let parsed_key = match super::models::parse_key(key){
            Ok(result) => result 
          , Err(e) => return Ok( Response::builder()
                                .status(StatusCode::BAD_REQUEST)
                                .body("".into())
                               )
          };
        //Check if the key is the test key
        if parsed_key == super::models::parse_key(super::TEST_KEY_PUBLIC_STR.to_string()).unwrap() {
            let board = super::models::generate_test_board();
            let reply = Response::builder()
                .header("Spring-Version", "83")
                .header("Content-Length", board.body.len())
                .header("Content-Type", "text/html;charset=utf-8")
                .header("Authorization", format!("Spring-83 Signature={}", hex::encode(board.signature)))
                .body(board.body.clone());
            return Ok(reply);
        }
        
        //Then check if the key is in the database
        let btm = db.lock().await;
        let board = match btm.get(&parsed_key[..]){
            Some(result) => result
          , None => return Ok( Response::builder()
                              .status(StatusCode::NOT_FOUND)
                              .body("".into())
                             )
          };

        match if_modified_since {
            Some(possible_date_string) => {
                let parsed_date_string = match parse_http_date(&possible_date_string){
                    Ok(result) => result
                  , Err(e) => return Ok( Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body("".into())
                                       )
                };
                if board.last_modified < parsed_date_string {
                    return Ok( Response::builder()
                              .status(StatusCode::NOT_MODIFIED)
                              .body("".into())
                             )
                } 
            }
          , None => ()
          };
        let reply = Response::builder()
          .header("Spring-Version", "83")
          .header("Content-Length", board.body.len())
          .header("Content-Type", "text/html;charset=utf-8")
          .header("Authorization", format!("Spring-83 Signature={}", hex::encode(board.signature)))
          .body(board.body.clone());
        Ok(reply)
    }
}

mod models {
    use std::convert::Infallible;
    use std::sync::Arc;
    use std::collections::BTreeMap;
    use tokio::sync::Mutex;
    use regex::Regex;
    use hex::FromHexError::InvalidStringLength; 
    use std::time::SystemTime;
    use warp::http::Response;
    use ring::signature;


    type BoardID = [u8;32];
    pub type Db = Arc<Mutex<BTreeMap<BoardID, Board>>>;

    pub fn blank_db() -> Db {
        Arc::new(Mutex::new(BTreeMap::new()))
    }

    pub fn calculate_difficulty_factor(btm: &BTreeMap<BoardID, Board>) -> f32 {
        let len = btm.len();
        f32::powf(len as f32 * (1.0 / 10000000.0), 4.0)
    }

    /// Returns true if the string is a valid key,
    /// false if not
    pub fn parse_key(key: String) -> Result<Vec<u8>, hex::FromHexError>{
        let valid_ed = Regex::new(r"ed[0-9]{4}$").unwrap();
        if key == super::TEST_KEY_PUBLIC_STR {
            return hex::decode(key);
        }
        let decoded_key = match hex::decode(key) {
            Ok(result) => result, 
            Err(e) => return Err(e)
        };
        if decoded_key.len() != 32 {
            return Err(InvalidStringLength);
        }
        let expiration_date = &decoded_key[29..];
        let expiration_date = hex::encode(expiration_date);
        if ! valid_ed.is_match(&expiration_date){
            return Err(InvalidStringLength);
        }
        let expiration_date = &decoded_key[30..];
        let expiration_date = hex::encode(expiration_date);
        let expiration_date = expiration_date.parse::<i32>().unwrap();
        if expiration_date < 2022 || expiration_date > 2099 {
            return Err(InvalidStringLength);
        }
        Ok(decoded_key)
    }

    
    #[derive(Debug, Clone)]
    pub struct Board {
        pub key: [u8;32],
        pub last_modified: SystemTime,
        pub body: String,
        pub signature: [u8;64]
    }

    pub fn generate_board_response(board: &Board) -> Result<impl warp::Reply, Infallible> {
        let reply = Response::builder()
                    .header("Spring-Version", "83")
                    .header("Content-Length", board.body.len())
                    .header("Content-Type", "text/html;charset=utf-8")
                    .header("Authorization", format!("Spring-83 Signature={}", hex::encode(board.signature)))
                    .body(board.body.clone());
        Ok(reply)
    }

    pub fn generate_test_board() -> Board {
        let keypair: signature::Ed25519KeyPair = generate_test_keypair();
        let last_modified = std::time::SystemTime::now();
        let body = format!( "<html><body>{}</body></html>"
                          , httpdate::fmt_http_date(last_modified.clone())
                          ); 
        let signature = keypair.sign(&body[..].as_bytes());

        Board {
            key: [0; 32]
          , last_modified: last_modified
          , body: body
          , signature: signature.as_ref().try_into().unwrap()
        }
    }

    pub fn generate_test_keypair() -> signature::Ed25519KeyPair {
        let test_key_public_bytes = hex::decode(super::TEST_KEY_PUBLIC_STR).unwrap();
        let test_key_secret_bytes = hex::decode(super::TEST_KEY_SECRET_STR).unwrap();
        signature::Ed25519KeyPair::from_seed_and_public_key(
            &test_key_secret_bytes
          , &test_key_public_bytes
          ).unwrap()
    }

}


#[cfg(test)]
mod tests {
    use warp::http::StatusCode;
    use warp::test::request;
    use hex::FromHexError::InvalidStringLength; 

    use super::{
        filters,
        models::{self, Board},
    };

    #[tokio::test]
    async fn test_get_difficulty_factor_endpoint(){
        let db = models::blank_db();
        db.lock().await.insert([0; 32], board1());
        let api = filters::get(db);
        let resp = request()
            .method("GET")
            .path("/")
            .reply(&api)
            .await;
        // Requests with an incorrect header receive 400
        assert_eq!(resp.status(), StatusCode::BAD_REQUEST);

        let resp = request()
            .method("GET")
            .path("/")
            .header("Spring-Version", "83")
            .reply(&api)
            .await;
        // A successful request has the following headers
        assert_eq!(resp.status(), StatusCode::OK);
        assert_eq!(resp.headers()["Spring-Version"], "83");
        assert_eq!(resp.headers()["Spring-Difficulty"], "0.000000000000000000000000000100000006");
        assert_eq!(resp.headers()["Content-Length"], "39");
    }

    #[tokio::test]
    async fn test_calculate_difficulty_factor(){
        let db = models::blank_db();
        let mut dbm = db.lock().await;
        assert_eq!(
            models::calculate_difficulty_factor(&dbm)
          , 0.0
          );
        dbm.insert([0; 32], board1());
        assert_eq!(
            models::calculate_difficulty_factor(&dbm)
          , 1.00000006e-28
          );
    }

    #[test]
    fn test_valid_key(){
        //Must be 64 characters long
        let key = "00".to_string();
        assert_eq!(models::parse_key(key), Err(InvalidStringLength));
        //Valid if it's the test_key
        let key = super::TEST_KEY_PUBLIC_STR.to_string();
        assert_eq!(models::parse_key(key), hex::decode(super::TEST_KEY_PUBLIC_STR));
        let key = "0000000000000000000000000000000000000000000000000000000000ed2021".to_string();
        assert_eq!(models::parse_key(key), Err(InvalidStringLength));
        let key = "0000000000000000000000000000000000000000000000000000000000ed2100".to_string();
        assert_eq!(models::parse_key(key), Err(InvalidStringLength));
        //Valid if its the right length, and has the 4-digit year within the limits
        let key = "0000000000000000000000000000000000000000000000000000000000ed2022".to_string();
        let mut test_key = vec![0;32];
        test_key[29] = 0xED;
        test_key[30] = 0x20;
        test_key[31] = 0x22;
        assert_eq!(models::parse_key(key), Ok(test_key));
    }

    fn board1() -> Board {
        Board {
            key: [0; 32]
          , last_modified: std::time::SystemTime::now()
          , body: "".into()
          , signature: [0; 64]
        }
    }
}